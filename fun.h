/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   fun.h
 * Author: Przemysław Rzepka
 *
 * Created on 9 marca 2017, 17:04
 */

#ifndef FUN_H
#define FUN_H

#include <vector>
#include <bitset>
#include <string>
#include <ios>
#include <fstream>
#include <algorithm>
#include <memory>

using namespace std;

vector<bitset<8>> loadFile(){
    
    vector<bitset<8>> retVector;
    bitset<8> temp;
    streampos begin, end, sof;
    
    //  otwarcie pliku
    ifstream file ("data.dat", ios::binary);

    //  obliczenie rozmiaru pliku
    begin = file.tellg();
    file.seekg (0, ios::end);
    end = file.tellg();
    file.seekg(0,ios::beg);

//    cout << "size is: " << (end-begin) << " bytes.\n";
    
    //  obliczenie rozmiaru pliku i rzutowanie na integer
    sof=(end-begin);
    int size=(int)sof;
    
    //  stworzenie dynamicznie bufora danych 
    char *buffer = new char[size];
    
    //  zapis danych do bufora
    file.read(buffer,size);
   

    //  kopiowanie danych z bufora do vectora bitsetow
    for(int i=0; i!=size; i++){
        
        cout<<buffer[i];
      
        temp=buffer[i];
        retVector.push_back(temp);

 
    }
    
    cout<<endl;  

    delete[] buffer;
    file.close();
    
    return retVector;
    
}

//  //   // kodowanie slowa 8 bitowego z 8 bitami parzystosci
bitset<16> encode(bitset<8> T, vector<bitset<8>> H){
    
    bitset<16> retBitset;
    bitset<1> parityBit;
    bitset<8> overloadBits;
    
    //  oblcizanie bitow parzystosci
    for(int i=7; i!=-1; i--){
        int it = 0;
        
        // obliczanie konkretnego bitu parzystosci
        for(int ii=7; ii!=-1; ii--){ 
            
            //  dalsze obliczenia
           if(H[i][ii]==1 && it>0){
             
                parityBit[0] = parityBit[0] xor T[ii]; 
                                   
           } 

           //   przy pierwszej iteracji ustawiany jest pierwszy bit do dalszych obliczen***
           else if(H[i][ii]==1 && it==0){
               
               parityBit[0]=T[ii];
               it++;
           }
           
        }
        
        //  przypisanie obliczonego bitu parzystosci
        overloadBits[i]=parityBit[0];
    }
    
    // // //zapis do retBitset
    // za pomoca zmiennej 'o' ustawiamy bity nadmiarowe w poprawnej kolejnosci.
    // (po wyliczeniu w zmiennej 'overloadBits' ustawione sa one w odwrotnej kolejnosci)

    int o = 7;
    for(int i = 0; i<8; i++){
        
        retBitset[i] = overloadBits[o];
        retBitset[i+8] = T[i];
        o--;
        
    }
    
    return retBitset;
}

//  //  //  wynik wyniku przemnozenia matrycy H i wektora T (slowa)  
bitset<8> HTresult(vector<bitset<8>> H, bitset<16> word){
    
    //  ponizeszj obliczany jest wynik dzialania HT
    //  gdy wynik jest rozny zero to slowo jest poprawne
    int result;
    bitset<8> retVal;
    
    int j = 0;
    
    for(int i=7; i!=-1; i--){

        result=0;
        
        for(int ii=7; ii!=-1; ii--){
                
                result = result + (word[ii+8] * H[j][ii]); 
       
        }
        
        // dodanie do wyniku odpowiedniego bitu nadmiarowego
        // oraz dzialanie modulo 2
        result = result + word[i];
        result = result % 2;

        retVal[i]=result;
        
        j++;
        
    }
  
    return retVal;    
}

//  //  //  detekcja 2 bledow
vector<int> find2errors(vector<bitset<8>> H, bitset<8> HT){
    
    vector<int> badColumns {0,0,0};
    
    bitset<8> cmp;
    string HTres=HT.to_string();    //  zapis wyniu iloczynu HT do string
    string comp;
    int x = 0;                      //  zmienna pilnujaca poprawnej kolejnosci zapisywania wyniku sumowania kolumn
    

    // zmiana pierwszej kolumny
    for(int i=7; i!=-1; i--){


        // zmiana kolumny drugiej
        for(int ii=7; ii!=-1; ii--){

            x = 0;
            
            //  zmiana wersow w kolumnie pierwszej i drugiej oraz zapis ich sumy do 'cmp'
            for(int j=7; j!=-1; j--){

                cmp[x] = H[j][i] xor H[j][ii];
                x++;
            } 
            
            //  zapis 'cmp' do stringa 'comp' w celu porownania
            comp=cmp.to_string();
            
            
            // w przypadku detekcji zapis miejsc kolumn (blednych miejsc) do zmienych oraz dodanie ich do vectora
            if(comp==HTres){
                             
//                cout<<comp<<endl;
                
                badColumns.clear();
                badColumns.push_back(2);    //  flaga wskazujaca na wystapienie dwoch bledow
                badColumns.push_back(i+8);
                badColumns.push_back(ii+8);
                                
                return badColumns;
            }

        }

    }
    
    //  zwracamy wektor z miejscami blednych kolumn
    return badColumns;
}

//  //  //  naprawa 2 bledow
// przekazywany jest vector z numeracja blednych miejsc
bitset<16> correct2(bitset<16> word, vector<int> wrongBits){
    
    //  zanegowanie bitow ktorych pozycje wskazuja zmienne wrongBits[1] oraz [2]
    word[wrongBits[1]]=word[wrongBits[1]] xor 1;
    word[wrongBits[2]]=word[wrongBits[2]] xor 1;

    return word;
}

//  //  //  detekcja 1 bledu
vector<int> find1error(vector<bitset<8>> H, bitset<8> HT){
    
    vector<int> retVec {0,0,0};
    
    bitset<8> tmpHbits;
    string tmpHTres, tmpH;
    int x = 0;
    
    tmpHTres=HT.to_string();
    
    //  zmiana kolumny H
    for(int i = 7; i!=-1; i--){
        
        x = 0;
        
        //  zmiana wersu matrycy H
        for(int ii = 7; ii!=-1; ii--){
            
            //  zapisanie kolumny do zmiennej tmpHbits
            tmpHbits[x]=H[ii][i];
           
            x++;
        }
       
        //  zapisanie wyniku do stringa i porownanie go z wynikiem HT
        tmpH=tmpHbits.to_string();      
        if(tmpHTres==tmpH){
            
            retVec.clear();
            retVec.push_back(1);    //  flaga detekcji bledu
            retVec.push_back(i+8);  //  numer kolumny(bitu) na ktorym znajduje sie blad
            retVec.push_back(0);

            return retVec;
        }
    }
    
    return retVec;
    
}

//  //  //  naprawa jednego bledu
bitset<16> correct1(bitset<16> word, vector<int> wrongBits){
      
    word[wrongBits[1]]=word[wrongBits[1]] xor 1;
    
    return word;
}

//  //  //  dekodowanie slowa 16 bitowego 
char decode(bitset<16> word){
    
    int castInt;
    char retChar;
    bitset<8> sign;
    
    //  usuniecie bitow nadmiarowych ze slowa
    for(int i = 7; i!=-1; i--){
        sign[i]=word[i+8];
    }
    
    castInt=sign.to_ulong();    //  zapis slowa jako zmienna typu long
    
    retChar=(char)castInt;      //  rzutowanie slowa w postaci long na char
        
    return retChar;
    
}

//  //  //  zapisanie zakodowanych danych do pliku
void saveEncoded(vector<bitset<16>> encodedVec){
    
    vector<string> stringVector;
    string temp;
    bitset<16> tempBit;
    
    for(int i=0;i!=encodedVec.size();i++){

        stringVector.push_back(encodedVec[i].to_string());
    }

    ofstream file("encodedData.dat", ios::out );
    
    for(int i = 0; i!=stringVector.size(); i++){
    
        file<<stringVector[i]<<endl;
        
    }
    
    file.close();
}

//  //  //  odczyt zakodowanych z pliku (mozliwosc wpisania wczesniej bledu)
vector<bitset<16>> loadEncoded(){
    
    vector<shared_ptr<bitset<16>>>  sharedPtrVec;
    vector<bitset<16>> retVec;
    
    string temp;
    char tempCh[16];
            
    fstream file ("encodedData.dat", ios::in);
    
    while(!file.eof()){
        getline(file,temp);


        sharedPtrVec.push_back(shared_ptr<bitset<16>>(new bitset<16>(temp)));

    }
    
    sharedPtrVec.pop_back();
    
    // przepisanie wektora wskaznikow do wektora bitsetow
    for(int i = 0; i!=sharedPtrVec.size();i++){
        

        retVec.push_back(*sharedPtrVec[i]);
        
    }
    
    file.close();
    
    return retVec;
}

//  //  //  zapisanie zdekodowanych danych do pliku
void saveDecoded(vector<char> encodedVec){
    
     ofstream file("decodedData.dat", ios::out);
    
    for(int i = 0; i!=encodedVec.size(); i++){
    
        file<<encodedVec[i];
        
    }
    
    file.close();
}


#endif /* FUN_H */

