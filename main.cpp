/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Przemysław Rzepka
 *
 * Created on 9 marca 2017, 17:01
 */

#include <cstdlib>
#include <iostream>
#include <cstdio>
#include <bitset>
#include <vector>


#include "../CppApp_Telekom1/fun.h"

using namespace std;


// deklaracja poszeczególnych wersów wektora parzystości H
const bitset<8> H1 (0b11000101);
const bitset<8> H2 (0b11100010);
const bitset<8> H3 (0b01110001);
const bitset<8> H4 (0b10111000);
const bitset<8> H5 (0b01011100);
const bitset<8> H6 (0b00101110);
const bitset<8> H7 (0b00010111);
const bitset<8> H8 (0b10001011);

// deklaracja stałego vectora H
const vector<bitset<8>> H {H1,H2,H3,H4,H5,H6,H7,H8};

/*
 * 
 */

int main(int argc, char** argv) {

    char choice;                        //  wybor w menu
    int doubleError = 0;                //  licznik podwojnych bledow
    int singleError = 0;                //  licznik pojedyncych bledow
    vector<bitset<8>> rawWords;         //  wektor wiadomosci do kodowania
    vector<bitset<16>> encodedWords;    //  wektor zakodowanej wiadomosci
    vector<char> decodedWords;          //  wektor odkodowanej wiadomosci

    vector<int> errorFlags ;            //  wektor flag bledow
    
    vector<bitset<8>> tempVec;          //  wektor tymczasowy 'temp'
    
    bitset<8> tempHT;                   //  tymczasowy wynik H*T
    bitset<8> correctHTres = 0b00000000;//  poprawny wynik H*T
    bitset<16> temp16;                  //  tymczasowe slowo
       
    
    while(choice!='q' and choice !='Q'){
        
         system("cls");
        
            cout<<"Menu: \n"
                <<"1 - Wczytaj i zakoduj wiadomosc \n\\\\ nastepnie recznie wprowadz bledy edytujac plik \"encodedData.dat\" \n\n"
                <<"2 - Wczytaj wiadomosc i wyslij (bez korekty) \n"
                <<"3 - Wczytaj wiadomosc i wyslij skorygowany \n "
                <<"Press 'Q' to quit \n";
            cin>>choice;
            
            switch(choice){
                case '1':
                    encodedWords.clear();    //  wyczyszczenie wektora w celu unikniecia zdublowania danych
                    rawWords=loadFile();    //  wczytanie surowych slow do wektora bitsetow<8> z pliku 'data.dat'
                    
                    //  zakodowanie surowych slow i zapisanie ich do wektora 'encodedWords'
                    for(int i = 0; i!=rawWords.size(); i++){
                        encodedWords.push_back(encode(rawWords[i],H));
                    }
                    
                    saveEncoded(encodedWords);  //  zapisanie zakodowanych slow do pliku 'encodedData.dat'
                    
                    break;
                    
                case '2':
                    encodedWords=loadEncoded();   //  wczytanie zakodowanych slow do wektora 'encodedWords' z pliku 'encodedData.dat
                    decodedWords.clear();   //  wyczyszczenie wektora w celu unikniecia zdublowania danych
                    
                    //  wyswietlenie wiadomosci oraz przepisanie jej do wektora 'decodedWords'
                    cout<<"WIADOMOSC: \n";
                    
                    for(int i=0;i!=encodedWords.size();i++){
                        
                        decodedWords.push_back(decode(encodedWords[i]));
                        cout<<decodedWords[i];
                        
                    }
                    
                    cout<<endl;
                    
                    saveDecoded(decodedWords);  //  zapisanie wektora 'decodedWords'
                    
                    break;
                    
                    
                    
                case '3':
                    encodedWords=loadEncoded();   //  wczytanie zakodowanych slow do wektora 'encodedWords' z pliku 'encodedData.dat

                    
                    //  //  //  DETEKCJA ORAZ KOREKCJA BLEDU/OW
                    for(int i = 0; i!=encodedWords.size(); i++){
                      
                        tempHT=HTresult(H,encodedWords[i]);     //  zapisanie wyniku H*T do zmiennej 'tempHT'
                        temp16=encodedWords[i];                 //  zapisanie slowa do zmiennej 'temp16'

                        //  sprawdzenie czy wynik jest rozny od poprawnego 0b00000000
                        if(tempHT!=correctHTres){
                        
                            errorFlags.clear();     //  wyczyszczenie flag 
                                                    
                            errorFlags=find2errors(H,tempHT);    //  szukanie 2 bledow i zapisanie ich pozycji w zmiennej 'errorFlags'

                            //  sprawdzenie czy znaleziono dwa bledy
                            // errorFlag[0] pokazuje ilosc znalezionych bledow
                            if(errorFlags[0]==2){
                                
                                encodedWords.erase(encodedWords.begin()+i);         //  usuniecie blednego slowa z wektora
                                temp16=correct2(temp16,errorFlags);               //  przypisanie poprawnego slowa do zmiennej 'temp16'
                                encodedWords.insert(encodedWords.begin()+i,temp16); //  wstawienie 'temp16' do wektora
                                
                                doubleError++;  //  inkrementacja licznika podwojnych bledow
                            }
                            //  w przypadku gdy liczba bledow jest inna niz 2, szukamy bledu pojedynczego
                            else{
                                
                                errorFlags.clear();                                 //  czyszczenie wektora flag
                                errorFlags=find1error(H,tempHT);             //  szukanie pojedynczego bledu
                                
                                encodedWords.erase(encodedWords.begin()+i);         //  usuniecie blednego slowa                        
                                temp16=correct1(temp16,errorFlags);               //  przypisanie poprawionego slowa do zmiennej                             
                                encodedWords.insert(encodedWords.begin()+i,temp16); //  wstawienie zmiennej do wektora
                                   
                                singleError++;  //  inkrementacj licznika pojedynczych bledow
                            }
  
                        }
                        
                    }
                    
                    //  jezeli suma licznikow bledow jest niezerowa to wyswietlany jest komunikat podsumowujacy
                    if(singleError+doubleError!=0){
                        cout<<"\nWykryto "
                            <<singleError+doubleError
                            <<" bledow, w tym "
                            <<singleError
                            <<" pojedynczych oraz "
                            <<doubleError
                            <<" podwojnych \n\n";
                    }
                        
                    //  //  //  KONIEC DETEKCJI BLEDOW  //  //  //
                    
                    decodedWords.clear();   //  wyczyszczenie wektora w celu unikniecia zdublowania danych
                    
                    //  wyswietlenie wiadomosci oraz przepisanie jej do wektora 'decodedWords'
                    cout<<"WIADOMOSC: \n\n";
                    
                    for(int i=0;i!=encodedWords.size();i++){
                        
                        decodedWords.push_back(decode(encodedWords[i]));
                        cout<<decodedWords[i];
                        
                    }
                    
                    cout<<"\n\n";
                    
                    saveDecoded(decodedWords);  //  zapisanie wektora 'decodedWords'
                    
                    break;
          
        }
     
         system("pause");
    }       
            
    return 0;
}

